<?php
/**
 * Created by PhpStorm.
 * User: toshiba
 * Date: 3/21/2018
 * Time: 2:28 PM
 */
namespace Pondit\Calculator\VolumeCalculator;


class Volume
{
    public $width ;
    public $length;
    public $height;

    public function getVol()
    {
        return $this->width * $this->length * $this->height;
    }
}